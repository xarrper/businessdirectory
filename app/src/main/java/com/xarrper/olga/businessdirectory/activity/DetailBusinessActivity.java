package com.xarrper.olga.businessdirectory.activity;

import android.support.v4.app.Fragment;

import com.xarrper.olga.businessdirectory.fragment.DetailBusinessFragment;

public class DetailBusinessActivity extends SingleFragmentActivity {

    @Override
    protected Fragment createFragment() {
        return new DetailBusinessFragment();
    }

}
