package com.xarrper.olga.businessdirectory.fragment;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.bignerdranch.android.multiselector.ModalMultiSelectorCallback;
import com.bignerdranch.android.multiselector.MultiSelector;
import com.bignerdranch.android.multiselector.SwappingHolder;
import com.xarrper.olga.businessdirectory.R;
import com.xarrper.olga.businessdirectory.activity.DetailBusinessActivity;
import com.xarrper.olga.businessdirectory.entity.Business;
import com.xarrper.olga.businessdirectory.helper.DatabaseHelper;

import java.util.ArrayList;

public class ListBusinessFragment extends Fragment {

    private static final String TAG = "ListBusinessFragment";
    protected RecyclerView recyclerView;
    protected ContentAdapter adapter;
    private MultiSelector multiSelector = new MultiSelector();
    public ArrayList<Business> businessList;
    private DatabaseHelper dbHelper;

    private static final int REQUEST_NEW_BUSINESS = 1;
    private static final int REQUEST_UPDATE_BUSINESS = 2;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        dbHelper = new DatabaseHelper(getContext());
        businessList = dbHelper.getAllBusinesses();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.list_business, parent, false);

        recyclerView = (RecyclerView)v.findViewById(R.id.my_recycler_view);
        adapter = new ContentAdapter();
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

        return v;
    }

    public class ContentAdapter extends RecyclerView.Adapter<ViewHolder> {

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.card_business, parent, false);
            return new ViewHolder(view);
        }

        @Override
        public void onBindViewHolder(ViewHolder holder, int position) {
            final Business business = businessList.get(position);
            holder.bindPointList(business);
        }

        @Override
        public int getItemCount() {
            return businessList.size();
        }

        @Override
        public void onAttachedToRecyclerView(RecyclerView recyclerView) {
            super.onAttachedToRecyclerView(recyclerView);
        }

    }

    public class ViewHolder extends SwappingHolder implements View.OnClickListener, View.OnLongClickListener {

        private final TextView name;
        private Business business;

        public ViewHolder(View itemView) {
            super(itemView, multiSelector);

            name = (TextView) itemView.findViewById(R.id.nameBusinessTextView);

            itemView.setOnClickListener(this);
            itemView.setLongClickable(true);
            itemView.setOnLongClickListener(this);
        }

        public void bindPointList(final Business business) {
            this.business = business;
            name.setText(business.getName());
        }

        @Override
        public void onClick(View view) {

            if (business == null) {
                return;
            }
            if (!multiSelector.tapSelection(this)) {
                Intent i = new Intent(getActivity(), DetailBusinessActivity.class);
                i.putExtra(DetailBusinessFragment.BUSINESS, business);
                i.putExtra(DetailBusinessFragment.POSITION, getPosition());
                startActivityForResult(i, REQUEST_UPDATE_BUSINESS);
            }

        }

        @Override
        public boolean onLongClick(View view) {
            ((AppCompatActivity) getActivity()).startSupportActionMode(deleteMode);
            multiSelector.setSelected(this, true);
            return true;
        }

    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.action_bar, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.add_business:
                Intent i = new Intent(getActivity(), DetailBusinessActivity.class);
                i.putExtra(DetailBusinessFragment.BUSINESS, new Business());
                startActivityForResult(i, REQUEST_NEW_BUSINESS);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void onActivityCreated(Bundle savedInstanceState) {
        if (multiSelector != null) {
            Bundle bundle = savedInstanceState;
            if (bundle != null) {
                //воостанавливаем вбранные элементы на view
                multiSelector.restoreSelectionStates(bundle.getBundle(TAG));
            }
            //если есть выбранные элементы
            if (multiSelector.isSelectable()) {
                //и удаление
                if (deleteMode != null) {
                    //восстанавливаем меню удаления
                    deleteMode.setClearOnPrepare(false);
                    ((AppCompatActivity) getActivity()).startSupportActionMode(deleteMode);
                }
            }
        }
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putBundle(TAG, multiSelector.saveSelectionStates());
        super.onSaveInstanceState(outState);
    }

    private ModalMultiSelectorCallback deleteMode = new ModalMultiSelectorCallback(multiSelector) {

        @Override
        public boolean onCreateActionMode(android.support.v7.view.ActionMode actionMode, Menu menu) {
            super.onCreateActionMode(actionMode, menu);
            getActivity().getMenuInflater().inflate(R.menu.context_delete, menu);
            return true;
        }

        @Override
        public boolean onActionItemClicked(android.support.v7.view.ActionMode actionMode, MenuItem menuItem) {
            if (menuItem.getItemId()==  R.id.menu_item_delete_crime){
                actionMode.finish();
                for (int i = businessList.size(); i >= 0; i--) {
                    if (multiSelector.isSelected(i, 0)) {
                        businessList.remove(i);
                        recyclerView.getAdapter().notifyItemRemoved(i);
                    }
                }
                multiSelector.clearSelections();
                return true;
            }
            return false;
        }

    };

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.d(TAG, "onActivityResult");
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            Business business = (Business) data.getSerializableExtra(DetailBusinessFragment.BUSINESS);
            if (requestCode == REQUEST_NEW_BUSINESS) {
                businessList.add(business);
                recyclerView.getAdapter().notifyDataSetChanged();
            }
            if (requestCode == REQUEST_UPDATE_BUSINESS) {
                int position = data.getIntExtra(DetailBusinessFragment.POSITION, 0);
                businessList.set(position, business);
                recyclerView.getAdapter().notifyDataSetChanged();
            }
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        dbHelper.clearBusiness();
        for (Business business : businessList) {
            dbHelper.insertBusiness(business);
        }
    }

}
