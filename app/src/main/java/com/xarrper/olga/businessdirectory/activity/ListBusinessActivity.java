package com.xarrper.olga.businessdirectory.activity;

import android.support.v4.app.Fragment;

import com.xarrper.olga.businessdirectory.fragment.ListBusinessFragment;

public class ListBusinessActivity extends SingleFragmentActivity {

    @Override
    protected Fragment createFragment() {
        return new ListBusinessFragment();
    }

}
