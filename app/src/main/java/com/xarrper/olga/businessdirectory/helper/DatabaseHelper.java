package com.xarrper.olga.businessdirectory.helper;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.xarrper.olga.businessdirectory.entity.Business;

import java.util.ArrayList;

public class DatabaseHelper extends SQLiteOpenHelper {

    private static final String DB_NAME = "businesses.sqlite";
    private static final int VERSION = 1;

    private static final String TABLE_BUSINESS = "business";
    private static final String COLUMN_BUSINESS_ID = "_id";
    private static final String COLUMN_BUSINESS_NAME = "name";
    private static final String COLUMN_BUSINESS_PHONE = "phone";
    private static final String COLUMN_BUSINESS_EMAIL = "email";
    private static final String COLUMN_BUSINESS_WEBSITE = "website";

    public DatabaseHelper(Context context) {
        super(context, DB_NAME, null, VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        db.execSQL("create table " + TABLE_BUSINESS + " (" +
                "_id integer primary key autoincrement, " +
                COLUMN_BUSINESS_NAME + " text, " + COLUMN_BUSINESS_PHONE + " text, " +
                COLUMN_BUSINESS_EMAIL + " text, " + COLUMN_BUSINESS_WEBSITE + " text)");

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    public long insertBusiness(Business business) {
        ContentValues cv = new ContentValues();
        cv.put(COLUMN_BUSINESS_NAME, business.getName());
        cv.put(COLUMN_BUSINESS_PHONE, business.getPhone());
        cv.put(COLUMN_BUSINESS_EMAIL, business.getEmail());
        cv.put(COLUMN_BUSINESS_WEBSITE, business.getWebsite());
        return getWritableDatabase().insert(TABLE_BUSINESS, null, cv);
    }

    public void clearBusiness() {
        String selectQuery = "DELETE FROM " + TABLE_BUSINESS + ";";
        SQLiteDatabase db = this.getReadableDatabase();
        db.execSQL(selectQuery);
    }

    public ArrayList<Business> getAllBusinesses() {
        ArrayList<Business> businesses = new ArrayList<Business>();
        String selectQuery = "SELECT DISTINCT * FROM " + TABLE_BUSINESS;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor c = db.rawQuery(selectQuery, null);
        if (c.moveToFirst()) {
            do {
                long id = c.getLong(c.getColumnIndex(COLUMN_BUSINESS_ID));
                String name = c.getString(c.getColumnIndex(COLUMN_BUSINESS_NAME));
                String phone = c.getString(c.getColumnIndex(COLUMN_BUSINESS_PHONE));
                String email = c.getString(c.getColumnIndex(COLUMN_BUSINESS_EMAIL));
                String website = c.getString(c.getColumnIndex(COLUMN_BUSINESS_WEBSITE));
                Business business = new Business(id, name, phone, email, website);
                businesses.add(business);
            } while (c.moveToNext());
        }
        return businesses;
    }

//    public int updateBusiness(Business business) {
//        ContentValues cv = new ContentValues();
//        cv.put(COLUMN_BUSINESS_NAME, business.getName());
//        cv.put(COLUMN_BUSINESS_PHONE, business.getPhone());
//        cv.put(COLUMN_BUSINESS_EMAIL, business.getEmail());
//        cv.put(COLUMN_BUSINESS_WEBSITE, business.getWebsite());
//        return getWritableDatabase().update(TABLE_BUSINESS, cv, COLUMN_BUSINESS_ID + " = ?",
//                new String[] { String.valueOf(business.getId()) });
//    }

}
