package com.xarrper.olga.businessdirectory.fragment;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.xarrper.olga.businessdirectory.R;
import com.xarrper.olga.businessdirectory.entity.Business;

public class DetailBusinessFragment extends Fragment {

    public static final String BUSINESS = "com.xarrper.olga.businessdirectory.fragment.business";
    public static final String POSITION = "com.xarrper.olga.businessdirectory.fragment.position";
    private Business business;
    private int position;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        business = (Business)getActivity().getIntent().getSerializableExtra(BUSINESS);
        position = getActivity().getIntent().getIntExtra(POSITION, 0);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.detail_businnes, container, false);

        final EditText nameEditText = (EditText) view.findViewById(R.id.nameEditText);
        nameEditText.setText(business.getName());
        final EditText phoneEditText = (EditText) view.findViewById(R.id.phoneEditText);
        phoneEditText.setText(business.getPhone());
        final EditText emailEditText = (EditText) view.findViewById(R.id.emailEditText);
        emailEditText.setText(business.getEmail());
        final EditText websiteEditText = (EditText) view.findViewById(R.id.websiteEditText);
        websiteEditText.setText(business.getWebsite());

        Button giveRingButton = (Button) view.findViewById(R.id.giveRingButton);
        giveRingButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                call(phoneEditText.getText().toString());
            }
        });
        Button displayLocationButton = (Button) view.findViewById(R.id.displayLocationButton);
        displayLocationButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                displayLocation(60, 30);
            }
        });
        Button writeLetterButton = (Button) view.findViewById(R.id.writeLetterButton);
        writeLetterButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendEmail(emailEditText.getText().toString());
            }
        });
        Button goToWebsiteButton = (Button) view.findViewById(R.id.goToWebsiteButton);
        goToWebsiteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getWebSite(websiteEditText.getText().toString());
            }
        });

        Button saveButton = (Button) view.findViewById(R.id.saveButton);
        saveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                business.setEmail(emailEditText.getText().toString());
                business.setName(nameEditText.getText().toString());
                business.setWebsite(websiteEditText.getText().toString());
                business.setPhone(phoneEditText.getText().toString());
                sendResult(Activity.RESULT_OK);
                getActivity().finish();
            }
        });


        return view;

    }

    private void call(String phone) {
        Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse(phone));
        intent.setData( Uri.parse("tel:" + phone));
        try{
            startActivity(intent);
        } catch (android.content.ActivityNotFoundException ex){
            Toast.makeText(getActivity(),"yourActivity is not founded",Toast.LENGTH_SHORT).show();
        }
    }

    private void sendEmail(String email) {
        String subject = "Subject";
        String text = "text";

        Intent emailIntent = new Intent(Intent.ACTION_SENDTO, Uri.fromParts(
                "mailto",email, null));
        emailIntent.putExtra(Intent.EXTRA_SUBJECT, subject);
        emailIntent.putExtra(Intent.EXTRA_TEXT, text);
        try {
            startActivity(Intent.createChooser(emailIntent, "Send email..."));
        } catch (android.content.ActivityNotFoundException ex) {
            Toast.makeText(getActivity(), "There are no email clients installed.", Toast.LENGTH_SHORT).show();
        }
    }

    private void getWebSite(String url) {
        Intent i = new Intent(Intent.ACTION_VIEW);
        i.setData(Uri.parse(url));
        startActivity(i);
    }

    private void displayLocation(double latitude, double longitude) {
        String uri = "geo:" + latitude + ","
                +longitude + "?q=" + latitude
                + "," + longitude;
        startActivity(new Intent(android.content.Intent.ACTION_VIEW,
                Uri.parse(uri)));
    }

    private void sendResult(int resultCode) {
        Intent i = new Intent();
        i.putExtra(BUSINESS, business);
        i.putExtra(POSITION, position);
        getActivity().setResult(resultCode, i);
    }
}
