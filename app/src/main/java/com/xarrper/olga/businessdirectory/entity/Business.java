package com.xarrper.olga.businessdirectory.entity;

import java.io.Serializable;

public class Business implements Serializable {

    private long id;
    private String name;
    private String phone;
    private String email;
    private String website;

    public Business() {

    }

    public Business(String name, String phone, String email, String website) {
        this.name = name;
        this.phone = phone;
        this.email = email;
        this.website = website;
    }

    public Business(long id, String name, String phone, String email, String website) {
        this.id = id;
        this.name = name;
        this.phone = phone;
        this.email = email;
        this.website = website;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

}
